# README #

HomePage link: http://localhost:8080/RankingWebsite/website

![RankingWebsite.jpg](https://bitbucket.org/repo/jM7Rzy/images/1644477570-RankingWebsite.jpg)

Implemented:

*  Additional features to track website previous weekly visit count report from link provided in UI.

Improvement List:

*  Percentage of visit increased from previous week report for every record on retrieval at UI level.
*  Angular JS design for UI page